package juniter.suites;

import juniter.math.CalculatorTest;
import juniter.math.FractionTest;
import juniter.math.NumberTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;


//WERKT NOG NIET HOE HET MOET
@RunWith(JUnitPlatform.class)
@SelectClasses({CalculatorTest.class, NumberTest.class, FractionTest.class})
public class MathSuite {
}
