package juniter.people;

import juniter.Person;
import org.junit.jupiter.api.*;

import java.io.BufferedReader;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class PersonTest {

    private BufferedReader reader;

    @BeforeAll
    public static void initializeAllTheThings(){
        loadExpensiveResourcesForAllTheTests();
    }

    @Test //Hamcrest Assertion
    public void checkingName(){
        Person p = new Person("Jimmi", "Hendrix");
        assertThat(p.getName(), both(startsWith("Jimmi")).and(endsWith("Hendrix")));
    }

    @Test //Long test
    @Tag("Slow")
    public void loooooongTest(){

    }

    @AfterAll
    public static void destroyAllTheThings(){
        destroyExpensiveResourcesForAllTheTests();
    }






    private static void loadExpensiveResourcesForAllTheTests(){

    }
    private static void destroyExpensiveResourcesForAllTheTests(){

    }
}
