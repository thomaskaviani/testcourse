package juniter.math;

import juniter.Number;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class NumberTest {

    @ParameterizedTest
    @ValueSource(ints = {8,10,4,6,8,12,2,8,6,30,22})
    public void isOddShouldReturnTrueForOddNumbers(int number) {
        Assertions.assertTrue(Number.isOdd(number));
    }
}
