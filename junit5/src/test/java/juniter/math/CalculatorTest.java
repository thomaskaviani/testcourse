package juniter.math;

import juniter.Calculator;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {



    @Test //Standard example test
    public void addingNumbers(){
        int sum = new Calculator().add(5,3);
        Assert.assertEquals("The sum of numbers doesn't add up",8, sum);
    }

    @Test
    public void addingNumbersLambda(){
        assertTrue(Stream.of(1, 2, 3)
                .mapToInt(i -> i)
                .sum() > 5, () -> "Sum should be greater than 5");
    }

    @Test //Order tests or assertions by group (assertAll)
    public void groupAssertions(){
        int[] numbers = {0,1,2,3,4};
        assertAll("numbers",
                ()-> assertEquals(numbers[0], 0),
                ()-> assertEquals(numbers[1],1),
                ()-> assertEquals(numbers[2], 2)
        );
    }

    /* Ignored test (not skipped)

    @Test
    @Disabled("Test currently disabled")
    public void stupidTest(){
    }

     */



}
