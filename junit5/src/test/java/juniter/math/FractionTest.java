package juniter.math;

import juniter.Fraction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FractionTest {

    private static Fraction f;

    @BeforeAll
    public static void initialize(){
        f = new Fraction(1,2);
    }

    @Test
    public void constructorTest(){
        assertAll(
                ()-> assertEquals(1, f.getNumerator()),
                ()-> assertEquals(2, f.getDenominator())
        );
    }

    @Test
    public void toStringTest(){
        assertEquals("1/2", f.toString());
    }

    @Test
    public void reduceTest(){
        f = new Fraction(2,4);
        assertEquals(1, f.getNumerator());
        assertEquals(2, f.getDenominator());
    }

    @Test
    public void denominatorZeroThrowsAritmeticException() {
        assertThrows(Exception.class, () -> {
            new Fraction(1,0);
        });
    }

    @ParameterizedTest
    @MethodSource("provideAddArguments")
    public void addFractionsTest(int[] args) {
        Fraction f1 = new Fraction(1,4);
        Fraction f2add = new Fraction (args[0], args[1]);
        Fraction fActual = f1.add(f2add);
        Fraction fSolution = new Fraction( args[2], args[3] );

        assertAll(
                ()-> assertEquals(fSolution.getNumerator(), fActual.getNumerator()),
                ()-> assertEquals(fSolution.getDenominator(), fActual.getDenominator())
        );
    }



    @AfterAll
    public static void destroy(){
        f = null;
    }


    private static List<int[]> provideAddArguments() {
        //First two are args num and denom
        //Last two are solution num and denom
        List<int[]> list = new ArrayList<>();
        list.add(new int[]{1, 4, 1, 2});
        list.add(new int[]{2,4,3,4});
        list.add(new int[]{5,4,3,2});
        list.add(new int[]{8,4,9,4});

        return list;
    }
}
