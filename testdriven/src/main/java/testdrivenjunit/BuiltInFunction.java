package testdrivenjunit;

import java.util.function.Function;
import java.util.regex.Pattern;

public abstract class BuiltInFunction implements Function<String, Integer> {

    private final String name;
    private final Pattern pattern;

    protected BuiltInFunction(
            String name,
            String regex) {
        this.name = name;
        this.pattern = Pattern.compile(regex);
    }

    public String getName() {
        return name;
    }

    public Pattern getPattern() {
        return pattern;
    }

}


