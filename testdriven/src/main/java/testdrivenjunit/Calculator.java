package testdrivenjunit;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

public class Calculator {

    //I used a javascript engine to handle the expression
    private static final ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");

    public static String handle(String expression){

        if(expression == null){
            return "NULL";
        }
        String js_parsable_expression = expression
                .replaceAll("\\((\\-?\\d+)\\)\\^(\\-?\\d+)", "(Math.pow($1,$2))")
                .replaceAll("(\\d+)\\^(\\-?\\d+)", "Math.pow($1,$2)");
        try{
            return engine.eval(js_parsable_expression).toString();
        }catch(javax.script.ScriptException e1){
            return null; // Invalid Expression
        }

    }

    //Robin's implementation
    private Map<String, Integer> variables = new HashMap<>();
    private List<BuiltInFunction> builtInFunctions = Arrays.asList(
            new BuiltInFunction(
                    "sum",
                    "sum\\(\\p{Alnum}+, \\p{Alnum}+\\)"
            ) {
                @Override
                public Integer apply(String s) {
                    String argsRaw[] = s.split(",");
                    return Arrays.stream(argsRaw)
                            .mapToInt(a -> lookup(a.trim()))
                            .sum();
                }
            }
    );

    private int parseDeclaration(String s) {
        String[] declarationParts = s.split(" ");
        if (declarationParts.length != 3) {
            throw new IllegalArgumentException("Declaration should be of form 'X = value'");
        }
        if (!"=".equals(declarationParts[1])) {
            throw new IllegalArgumentException("Declaration should be of form 'X = value'");
        }
        int value = Integer.parseInt(declarationParts[2]);
        variables.put(
                declarationParts[0],
                value
        );
        return value;
    }

    private String flattenNestedExpressions(String s) {
        // we will re-write the input string by extracting the nested expression and replacing the original expression
        // by the evaluation result
        String copy = s;
        // first, check for nested functions
        boolean noneMatch;
        do {
            noneMatch = true;
            for (BuiltInFunction builtInFunction : builtInFunctions) {
                Matcher m = builtInFunction.getPattern()
                        .matcher(copy);
                if (m.find()) {
                    String match = m.group();
                    noneMatch = false;
                    String argList = match.substring(
                            match.indexOf('(') + 1,
                            match.lastIndexOf(')')
                    );
                    int evalResult = builtInFunction.apply(argList);
                    copy = copy.substring(
                            0,
                            m.start()
                    ) + evalResult + copy.substring(m.end());
                }
            }
        } while (!noneMatch);
        // parentheses precedence
        while (copy.contains("(") && copy.contains(")")) {
            int openParenthesisIdx = copy.lastIndexOf('(');
            int closeParenthesisIdx = copy.indexOf(
                    ')',
                    openParenthesisIdx
            );
            if (openParenthesisIdx >= 0 && closeParenthesisIdx >= openParenthesisIdx) {
                String nestedExpr = copy.substring(
                        openParenthesisIdx + 1,
                        closeParenthesisIdx
                );
                copy = copy.substring(
                        0,
                        openParenthesisIdx
                ) + eval(nestedExpr) + copy.substring(closeParenthesisIdx + 1);
            } else {
                throw new RuntimeException("Closing parenthesis appears before open parenthesis in " + copy);
            }
        }
        return copy;
    }

    private int parseSimpleExpression(String s) {
        String[] tokens = s.trim()
                .split("\\s+");
        if (tokens.length == 0) {
            throw new IllegalArgumentException();
        }
        boolean lastWasOperator = false;
        int runningTotal = lookup(tokens[0].trim());
        for (int i = 1; i < tokens.length; i++) {
            String token = tokens[i].trim();
            if ("+".equals(token)) {
                runningTotal += lookup(tokens[i + 1].trim());
                lastWasOperator = true;
            } else if ("-".equals(token)) {
                runningTotal -= lookup(tokens[i + 1].trim());
                lastWasOperator = true;
            } else if ("*".equals(token)) {
                runningTotal *= lookup(tokens[i + 1].trim());
                lastWasOperator = true;
            } else if ("/".equals(token)) {
                runningTotal /= lookup(tokens[i + 1].trim());
                lastWasOperator = true;
            } else {
                if (!lastWasOperator) {
                    throw new IllegalArgumentException("Invalid input string, each operator should be followed by a number");
                }
                lastWasOperator = false;
            }
        }
        if (lastWasOperator) {
            throw new IllegalArgumentException("Invalid input string, last operator is missing right operand");
        }
        return runningTotal;
    }

    public int eval(String s) {
        // check for variable declaration
        if (s.trim()
                .contains("=")) {
            return parseDeclaration(s);
        }
        // recursively evaluate nested expressions
        String copy = flattenNestedExpressions(s);
        // parse a normal expression
        return parseSimpleExpression(copy);
    }

    private int lookup(String x) {
        String trimmed = x.trim();
        try {
            return Integer.parseInt(trimmed);
        } catch (Exception e) {
            if (variables.containsKey(trimmed)) {
                return variables.get(trimmed);
            } else {
                throw new RuntimeException("Unknown symbol: " + x);
            }
        }
    }



}
