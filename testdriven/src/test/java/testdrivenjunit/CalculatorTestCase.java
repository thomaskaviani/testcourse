package testdrivenjunit;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorTestCase {

    Calculator calculator = new Calculator();

    @Test
    void oneNumberResultsInSum() {
        assertEquals(7, calculator.eval("7"));
    }

    @Test
    void multiDigitNumbersAdding(){
        assertEquals(30, calculator.eval("12 + 18"));
    }

    @Test
    void twoNegativeNumbers(){
        assertEquals(-11, calculator.eval("-3 - 8"));
    }

    @Test
    void addExpression(){
        assertEquals(11, calculator.eval("3 + 8"));
    }

    @Test
    void addExpressionWithNegatives(){
        assertEquals(5, calculator.eval("-3 + 8"));
    }

    @Test
    void substractExpression(){
        assertEquals(5, calculator.eval("8 - 3"));
    }

    @Test
    void substractExpressionNegativeNumbers(){
        assertEquals(-11, calculator.eval("-8 - 3"));
    }

    @Test
    void multiplyExpression(){
        assertEquals(24, calculator.eval("8 * 3"));
    }

    @Test
    void divideExpression(){
        assertEquals(4, calculator.eval("8 / 2"));
    }

    @Test
    void threeNumbersOperation(){
        assertEquals(40, calculator.eval("20 + 30 - 10"));
    }



}
