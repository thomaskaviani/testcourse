package jdbunit.database;

import jdbunit.entity.PersonEntityService;
import jdbunit.model.Person;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.dbunit.Assertion.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;

class PersonCRUDTestCase {

    @BeforeEach
    void initializeDatabase(){
        try {
            IDatabaseConnection connection = new DatabaseConnection(PersonEntityService.createConnection());

            //To get rid of annoying warnings in the terminal output
            DatabaseConfig dbConfig = connection.getConfig();
            connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
            connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

            IDataSet dataSet = new FlatXmlDataSetBuilder().build(new File("src/main/resources/dat.xml"));
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
        } catch (DatabaseUnitException | MalformedURLException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void savePersonTest() throws ParseException, MalformedURLException, DatabaseUnitException, SQLException {

        Person p = new Person("Thomas", "Jacobs", new SimpleDateFormat("yyyy-MM-dd").parse("1994-02-20"), "Kanaalstraat", "20", "Halle", "1500");

        PersonEntityService entityService = new PersonEntityService();
        entityService.save(p);

        IDatabaseConnection connection = new DatabaseConnection(PersonEntityService.createConnection());

        //To get rid of annoying warnings in the terminal output
        DatabaseConfig dbConfig = connection.getConfig();
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

        IDataSet dataSetExpected = new FlatXmlDataSetBuilder().build(new File("src/test/resources/savePersonDat.xml"));
        QueryDataSet dataSetActual = new QueryDataSet(connection);
        dataSetActual.addTable("people", "select firstName,lastName,street,number,city,postalcode,birthDate from people");

        assertEquals(dataSetExpected, dataSetActual);
    }

    @Test
    void findPersonTest() throws ParseException {

        Person expected = new Person("Thomas", "Kaviani", new SimpleDateFormat("yyyy-MM-dd").parse("1995-01-20"), "Claesplein", "10", "Lembeek", "1502");

        PersonEntityService entityService = new PersonEntityService();
        Person actual = entityService.find(2);

        assertAll(
                ()-> Assertions.assertEquals(expected.getFirstName(), actual.getFirstName()),
                ()-> Assertions.assertEquals(expected.getLastName(), actual.getLastName()),
                ()-> Assertions.assertEquals(expected.getBirthDate(), actual.getBirthDate()),
                ()-> Assertions.assertEquals(expected.getAddress().getStreet(), actual.getAddress().getStreet()),
                ()-> Assertions.assertEquals(expected.getAddress().getNumber(), actual.getAddress().getNumber()),
                ()-> Assertions.assertEquals(expected.getAddress().getCity().getName(), actual.getAddress().getCity().getName()),
                ()-> Assertions.assertEquals(expected.getAddress().getCity().getPostalCode(), actual.getAddress().getCity().getPostalCode())
        );
    }

    @Test
    void removePersonTest() throws ParseException, MalformedURLException, DatabaseUnitException, SQLException {

        Person toRemove = new Person(2,"Thomas", "Kaviani", new SimpleDateFormat("yyyy-MM-dd").parse("1995-01-20"), "Claesplein", "10", "Lembeek", "1502");

        PersonEntityService entityService = new PersonEntityService();
        entityService.remove(toRemove);

        IDatabaseConnection connection = new DatabaseConnection(PersonEntityService.createConnection());

        //To get rid of annoying warnings in the terminal output
        DatabaseConfig dbConfig = connection.getConfig();
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

        IDataSet dataSetExpected = new FlatXmlDataSetBuilder().build(new File("src/test/resources/removePersonDat.xml"));
        QueryDataSet dataSetActual = new QueryDataSet(connection);
        dataSetActual.addTable("people", "select firstName,lastName,street,number,city,postalcode,birthDate from people");

        assertEquals(dataSetExpected, dataSetActual);
    }

    @Test
    void updatePersonTest() throws ParseException, SQLException, DatabaseUnitException, MalformedURLException {

        Person toUpdate = new Person(2,"Thomas", "Kaviani", new SimpleDateFormat("yyyy-MM-dd").parse("1995-01-20"), "Kasteelstraat", "80", "Brussel", "1000");

        PersonEntityService entityService = new PersonEntityService();
        entityService.update(toUpdate);

        IDatabaseConnection connection = new DatabaseConnection(PersonEntityService.createConnection());

        //To get rid of annoying warnings in the terminal output
        DatabaseConfig dbConfig = connection.getConfig();
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

        IDataSet dataSetExpected = new FlatXmlDataSetBuilder().build(new File("src/test/resources/updatePersonDat.xml"));
        QueryDataSet dataSetActual = new QueryDataSet(connection);
        dataSetActual.addTable("people", "select firstName,lastName,street,number,city,postalcode,birthDate from people");

        assertEquals(dataSetExpected, dataSetActual);
    }

    @AfterAll
    static void destroyDatabase(){
        try {
            IDatabaseConnection connection = new DatabaseConnection(PersonEntityService.createConnection());

            //To get rid of annoying warnings in the terminal output
            DatabaseConfig dbConfig = connection.getConfig();
            connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
            connection.getConfig().setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());

            IDataSet dataSet = new FlatXmlDataSetBuilder().build(new File("src/main/resources/dat.xml"));
            DatabaseOperation.DELETE_ALL.execute(connection, dataSet);
        } catch (DatabaseUnitException | MalformedURLException | SQLException e) {
            e.printStackTrace();
        }
    }

}
