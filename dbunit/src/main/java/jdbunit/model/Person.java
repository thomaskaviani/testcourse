package jdbunit.model;

import java.util.Date;

public class Person {

    private Integer id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Address address;

    public Person() {
    }

    public Person(Integer id, String firstName, String lastName, Date birthDate, String street, String number, String cityName, String postalCode) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        City c = new City(cityName, postalCode);
        Address a = new Address(street, number, c);
        this.address = a;
    }

    public Person(String firstName, String lastName, Date birthDate, String street, String number, String cityName, String postalCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        City c = new City(cityName, postalCode);
        Address a = new Address(street, number, c);
        this.address = a;
    }

    public Person(String firstName, String lastName, Date birthDate, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String name() {
        return firstName + " " + lastName;
    }
}
