package jdbunit.repository;

import jdbunit.model.Person;

public interface PersonRepository {
    void save(Person person);
    Person find(int id);
    void remove(Person person);
    void update(Person person);
}
