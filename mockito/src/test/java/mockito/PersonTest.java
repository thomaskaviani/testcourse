package mockito;


import mockito.model.Person;
import mockito.model.Printer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PersonTest {

    @Mock
    Printer printer;

    @Captor
    ArgumentCaptor<Person> captor;

    @Test
    void personCanBePrintedWithStub(){
        when(printer.startPrinting()).thenReturn(true);
        Person person = new Person("Jimi", "Hendrix");
        assertTrue(person.print(printer));
    }

    @Test
    public void personCanBePrintedTestFinal(){
        when(printer.startPrinting()).thenReturn(true);
        Person person = new Person("Jimi", "Hendrix");
        assertTrue(person.print(printer));

        verify(printer, times(1)).addToQueue(captor.capture());
        verify(printer, times(1)).startPrinting();

        assertEquals("Jimi", captor.getValue().firstName);
        assertEquals("Hendrix", captor.getValue().lastName);
    }


}
