package mockito.model;


public class Person implements PrintJob {

    public final String firstName,lastName;

    public Person (String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public boolean print(Printer printer){
        printer.addToQueue(this);
        return printer.startPrinting();
    }
}
